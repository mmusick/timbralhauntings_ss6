
(
~numCoeff = 18;

b = Buffer.alloc(s, 1024, 1); // for sampling rates 44100 and 48000

z = {
	var in, fft, array;

	in = SoundIn.ar(0);
	fft = FFT( buffer: b, in: in, hop: 0.5, wintype: 1 /*hann*/ );
	array = MFCC.kr(fft, numcoeff: ~numCoeff );
	array.size.postln;

	Out.kr(20, array);
	Out.ar(10,Pan2.ar(in));

}.play;

c = Bus.new('control', 20, ~numCoeff );

m = {
	var ms, myWindSize;

	myWindSize = Window.availableBounds;
	w = Window.new( ~numCoeff + "MFCC coefficients",
		Rect(myWindSize.right+10, 400, 400, 400));

	ms = MultiSliderView.new(w, Rect(2, 2, w.bounds.width-2, w.bounds.height-2));

	ms.value_(Array.fill(~numCoeff , 0.0));
	ms.elasticMode = 1;
	ms.valueThumbSize_(5.0);
	ms.indexThumbSize_(20.0);
	ms.gap_(0);

	w.front;

r = {
		inf.do{
			c.getn(~numCoeff , { arg val; { ms.value_(val * 0.9) }.defer });
			0.04.wait; // 25 frames per second
		};

	}.fork;
};
m.play;


CmdPeriod.doOnce( {
	"ended".postln;
	c.free;
	m.free;
	r.stop;
	b.free;
	c.free;
	x.free;
	w.close;
});





)


// tidy up
(
c.free;
m.free;
r.stop;
b.free;
c.free;
z.free;
w.close;
)