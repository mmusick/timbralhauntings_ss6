
// check variable name.  Some are copied from Timbral Hauntings


// create an empty array to put MFCC features in.  (assumes 8 seconds at every 100ms)
~mfccArray = Array.fill2D(8*100, 14, { arg r, c; nil; });
~mfccArray = Array.fill2D(8*100, 14, { arg r, c; r; });
~mfccArray = Array.fill(8*100 + 2, { arg r; nil; });
~mfccArray  = Array.new;
// ~mfccArray = Array.new;
~mfccArray[0]

~mfccArray[2] = [1,2,3,4,5,6,7,8,9,10,11,12,13];

~mfcc_bus = Bus.control( s, 13 );



(
SynthDef(\storeMFCCs, {
	arg buffNum = 0, mfccBufNum = 0;
	var sig, fft_past, mfccs;

	sig = PlayBuf.ar(1, buffNum, doneAction: 2 );
	fft_past = FFT(
		buffer: LocalBuf(1024),
		in: sig,
		hop: 0.5,
		wintype: 1 /*hann*/
	);
	mfccs = MFCC.kr(fft_past, 13);

	// moving mean filter
	mfccs = MeanTriggered.kr(
		mfccs,
		trig: 1,
		// one second long
		length: (s.sampleRate/s.options.blockSize).round
	);

	// send it through a control bus output
	Out.kr(mfccBufNum, mfccs);

	// for auditioning the signal
	Out.ar(0, sig);
}).add;

~storeTask = Task({
	var waitTime=0.01;
	var timestamp, i=0;
	NodeWatcher.register(~getMFCCs = Synth(\storeMFCCs, [\buffNum, b.bufnum, \mfccBufNum, ~mfcc_bus]), true);
	timestamp = 0;
	{~getMFCCs.isPlaying}.value.postln;
	({~getMFCCs.isPlaying==true}).while({
		var mfcc;
		mfcc = ~mfcc_bus.getnSynchronous(13);
		// ~mfccArray[i]=mfcc ++ [timestamp];
		~mfccArray = ~mfccArray.add(mfcc ++ [timestamp]);
		// ~mfccArray[i].postln;
		i = i+1;
		timestamp=timestamp+waitTime;
		waitTime.wait;
	});
	"done".postln;
	~mfccArray.do({|i, r| i.postln;});
}).start;



)



~mfccArray[0] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
~mfccArray[0][1]
(
~mfccArray.do( {
	arg i, r;
	i.postln;
	r.postln;
});
)































~getMFCCs=nil

NodeWatcher.register(~getMFCCs = Synth(\storeMFCCs, [\buffNum, b.bufnum, \mfccBufNum, ~mfcc_bus]));

	~getMFCCs = Synth(\storeMFCCs, [\buffNum, b.bufnum, \mfccBufNum, ~mfcc_bus]);

~getMFCCs.isPlaying;



// record test audio
(
// allocate a Buffer
s = Server.local;
b = Buffer.alloc(s, s.sampleRate * 2.0, 1); // a four second 1 channel Buffer
)

b.numChannels
s.sampleRate
// record for four seconds
(
x = SynthDef(\help_RecordBuf, { arg out = 0, bufnum = 0;
	var formant;
	formant = Normalizer.ar( SoundIn.ar(0), level: 0.7, dur: 0.3 );
	// check the offset for different hardware/sampling rates
	// but i found that RecordBuf moved into the buffer that many samples before it starts recording
	RecordBuf.ar(formant, bufnum, offset: -26460, doneAction: 2, loop: 0);
}).play(s,[\out, 0, \bufnum, b]);
)
b.numFrames
b.getn(26460, 10, {|msg| msg.do({|i| i.postln}); });
b.plot;
// play it back
(
SynthDef(\help_RecordBuf_overdub, { arg out = 0, bufnum = 0;
    var playbuf;
    playbuf = PlayBuf.ar(1,bufnum);
    FreeSelfWhenDone.kr(playbuf); // frees the synth when the PlayBuf is finished
    Out.ar(out, playbuf);
}).play(s, [\out, 0, \bufnum, b]);
)
{BufRateScale.kr(b).poll;}.play


44100*0.01
(
SynthDef(\audioRecorder, {
	// bufnum is the buffer to write to
	// input is the audio input channel
	// delaycomp is to compensate for any delays introduced during pre-processing
	arg bufnum = 0, input = 0, delayComp = 0, t_reset = 0, gate = 1, tempBuff = 0;
	var inSig, playhead, recorder, countdown, env;

	env = Env.asr(0.5, 1, 1);
	env = EnvGen.kr(env, gate, doneAction: 2 );

	inSig = SoundIn.ar(input) * env;

	playhead = Phasor.ar(
		trig: t_reset,
		rate: 1,
		start: delayComp,
		end: BufFrames.kr(bufnum),
		resetPos: delayComp
	);

	// (playhead / s.sampleRate).poll;

	recorder = BufWr.ar( inSig, bufnum, playhead, 0 );
	recorder.poll;
	countdown = (2 + recorder - BufFrames.kr(bufnum));
	SendTrig.ar([K2A.ar(Done.kr(env)), countdown], 1, recorder);
	SendTrig.ar([K2A.ar(Done.kr(env)), countdown], 2, tempBuff);

	FreeSelf.kr(Done.kr(recorder.poll));

}).add;
)




b = Buffer.alloc(s, s.sampleRate * 20.0, 1); // a four second 1 channel Buffer
(
x = Synth(\audioRecorder, [\bufnum, b]);

o = OSCFunc({ arg msg, time;
    [time, msg].postln;
},'/tr', s.addr);
)

x.set(\gate, 0)
//
b.getn(165441, 100, { |arr| arr.do({|val, pos| pos.post; val.postln; }); });

b.plot
Trig


(
SynthDef(\compareMFCCs, {
	arg mfccFFT_size = 1024, numOfCoef = 13, pastBuffNum = 0, in = 0;
	var fft_past, fft_present, mfccArray_past, mfccArray_present, pastSig, inputSig;

	pastSig = PlayBuf.ar(1, pastBuffNum, loop: 1 );

	inputSig = SoundIn.ar(in);

	// get frequency domain
	fft_past = FFT(
		buffer: LocalBuf(mfccFFT_size),
		in: pastSig,
		hop: 0.5,
		wintype: 1 /*hann*/
	);

	fft_present = FFT(
		buffer: LocalBuf(mfccFFT_size),
		in: inputSig,
		hop: 0.5,
		wintype: 1 /*hann*/
	);


	// compute MFCCs
	mfccArray_past = MFCC.kr( fft_past, numcoeff: numOfCoef );
	mfccArray_present = MFCC.kr( fft_present, numcoeff: numOfCoef );






});


~der.value(3)

Synth(\test)

SynthDef(\test, {
	arg blah = 0;
	var ha;
	ha = 2+2+blah;
	^ha
}).add;


d = 1;

d = (d-1).abs

~micDelayBus.get

~tempRecBuffs[0].play
d = nil;
d.free;
d.plot
d.numFrames
e = 956481

Function

~tempRecBuffs[0].plot
(
{
	d = Buffer.alloc( s, e, 1 );
	0.1.wait;
	~tempRecBuffs[0].copyData(d);
}.fork;
)


428545/s.options.blockSize

d.plot
~tempMFCCBuffs[0].loadToFloatArray(action: { arg array; a = array; {a.plot;}.defer; "done".postln;});

Array
a[1000..1100]

~mfccBus.get
~tempRecBuffs[0].plot
~tempSlopeBuffs[1].plot
~tempMFCCBuffs[0].plot
e.plot
.get(200,  {|d| d.postln;};)

~tempRecSynths[~tempRecSynths[2]] = nil