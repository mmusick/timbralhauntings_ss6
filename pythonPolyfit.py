# !usr/bin/python

#************************************************************
#
#	Polyfit Coefficient Finder
#	
#	Michael Musick
#
# 		Description: Get the coefficients from np.polyfit
#					SC does not have a suitable polynomial
#					regression or OLS solver.  So call this
#					from a .unixCmd to get coeff for an array
#					
#					To be used with Timbreal Hauntings
#					
#		IMPORTANT: The path to this file needs to be specified
#					in the .scd file, in the 
#					~getPolyCoeff() function
#
#*************************************************************



import sys
import numpy as np
import warnings


# get the incoming array
incomingArray = sys.argv[1]
# check if a user specified a degree to find
try:
	degree = sys.argv[2]	
except Exception, e:
	degree = 5

# check if a user specified a degree to find
try:
	plot = int(sys.argv[3])
	# optional plotting
	if plot==1:
		import matplotlib.pyplot as plt
		plot = True
except:
	plot = False

def main( arr ):    
	array = np.array( eval(arr) )
	time = np.arange(0, array.size, 1)
	# print array
	# print time	

	coeff = np.polyfit(time, array, degree)
	
	# optional plotting
	if plot:
		fig, ax = plt.subplots(1)
		ax.plot( time, array, 'k-' )
		func = np.poly1d(coeff)
		step = 0.2
		x = np.arange(-2, array.size+1, step)
		ax.plot( x, func(x), 'r-' )
		ax.grid(True)
		fig.patch.set_facecolor('white')
		plt.show()

	return coeff
	# return "test"

warnings.filterwarnings("ignore")
if __name__ == "__main__":
    x = main( incomingArray )
    print( x.tolist() )
    

