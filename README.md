# Timbral Hauntings - Sonic Space no. 6 #
## 2014 ##

Code was originally developed with SuperCOllider 3.6.6 and Python 2

For more information please visit: http://michaelmusick.com/timbral-hauntings/

## Program Notes ##

Timbral Hauntings (Sonic Space No. 6 – Iteration No. 1) is an interactive installation work that borrows ideas from soundscape analysis and the idea that echoes from the past effect the present and future. Timbral Hauntings ‘listens’ to the soundscape of the space, analyzing the timbre of each acoustic event; cataloguing the eight most commonly occurring timbral phrases throughout the life of the system. As a critical number of events are collected, the system selects the most commonly occurring timbral phrase. This phrase is used to shape the timbre of the ‘present’ in the hauntings of the past. At the same time the selected phrase from the past is analyzed for near timbral matches from the present. When a match occurs, those near moments from the past are played back in an attempt to influence the future in repeating the past. As the composition progresses, new phrases from the past are selected, allowing for the constant progression in the emergent composition.

Participants to the either space are welcome to wander the space, simply sit and listen, or contribute to the composition, in any way they feel comfortable, including playing the instruments laid out.

Timbral Hauntings is part of Michael Musick’s Sonic Spaces Project.